package metroapi.util;
import java.util.Collection;
import java.util.HashMap;

public class DoubleEntryMap<K, V> {
	private HashMap<K, V> map = new HashMap<>();
	private HashMap<V, K> reverseMap = new HashMap<>();
	
	public void put(K key, V value) {
		map.put(key, value);
		reverseMap.put(value, key);
	}
	
	public K getKey(V value) {
		return reverseMap.get(value);
	}
	
	public V getValue(K key) {
		return map.get(key);
	}
	
	public Collection<K> getKeys() {
		return reverseMap.values();
	}
	
	public Collection<V> getValues() {
		return map.values();
	}
}
