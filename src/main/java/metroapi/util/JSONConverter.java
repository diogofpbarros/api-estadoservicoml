package metroapi.util;
import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONObject;

import metroapi.data.Day;
import metroapi.data.Destination;
import metroapi.data.MetroLine;
import metroapi.data.Stop;
import metroapi.data.StopTrainTimes;
import metroapi.data.TrainFrequency;

/**
 * 
 * Auxiliary class used to convert JSON objects.
 * 
 * @author Diogo F. P. Barros
 *
 */
public class JSONConverter {
	
	/*
	 * ######################################## PARSE ESTACAO ########################################
	 */
	
	public static List<Stop> convertJSONArrayToEstacaoList(JSONArray jsonArray) {
		ArrayList<Stop> ret = new ArrayList<>();

		for(int i = 0; i < jsonArray.length(); i++) 
			ret.add(convertJSONObjectToEstacao(jsonArray.getJSONObject(i)));
		
		return ret;
	}
	
	public static Stop convertJSONObjectToEstacao(JSONObject json) {
		return new Stop(json.getString("stop_id"),
						   json.getString("stop_name"),
						   json.getString("zone_id"),
						   parseMetroLineArray(json.getString("linha")),
						   parseStringArray(json.getString("stop_url")),
						   json.getDouble("stop_lat"),
						   json.getDouble("stop_lon"));
	}
	
	/*
	 * ######################################## PARSE STATION TIMES ########################################
	 */
	
	public static List<StopTrainTimes> convertJSONArrayToStationTimesList(JSONArray jsonArray) {
		ArrayList<StopTrainTimes> ret = new ArrayList<>();

		for(int i = 0; i < jsonArray.length(); i++) 
			ret.add(convertJSONObjectToStationTimes(jsonArray.getJSONObject(i)));
		
		return ret;
	}
	
	public static StopTrainTimes convertJSONObjectToStationTimes(JSONObject json) {
		int[] times = {
				json.getInt("tempoChegada1"),
				json.getInt("tempoChegada2"),
				json.getInt("tempoChegada3")
		};
		
		String[] trains = {
			json.getString("comboio"),
			json.getString("comboio2"),
			json.getString("comboio3")
		};
		
		return new StopTrainTimes(json.getString("stop_id"),
								json.getString("cais"),
								json.getString("hora"),
								times,
								trains,
								json.getInt("destino"),
								json.getString("sairServico"));
	}
	
	/*
	 * ######################################## PARSE DESTINATION ########################################
	 */
	
	public static List<Destination> convertJSONArrayToDestinationList(JSONArray jsonArray) {
		ArrayList<Destination> ret = new ArrayList<>();

		for(int i = 0; i < jsonArray.length(); i++) 
			ret.add(convertJSONObjectToDestination(jsonArray.getJSONObject(i)));
		
		return ret;
	}
	
	public static Destination convertJSONObjectToDestination(JSONObject json) {
		return new Destination(json.getInt("id_destino"),
							   json.getString("nome_destino"));
	}
	
	/*
	 * ######################################## PARSE TRAIN FREQUENCY ########################################
	 */
	
	public static List<TrainFrequency> convertJSONArrayToTrainFrequencyList(JSONArray jsonArray) {
		ArrayList<TrainFrequency> ret = new ArrayList<>();

		for(int i = 0; i < jsonArray.length(); i++) 
			ret.add(convertJSONObjectToTrainFrequency(jsonArray.getJSONObject(i)));
		
		return ret;
	}
	
	public static TrainFrequency convertJSONObjectToTrainFrequency(JSONObject json) {
		return new TrainFrequency(MetroLine.parseLinha(json.getString("Linha")),
								  json.getString("HoraInicio"),
								  json.getString("HoraFim"),
								  json.getString("Intervalo"),
								  json.getString("UT"),
								  Day.parseDay(json.getString("Dia")));
	}
	
	/*
	 * ######################################## AUX ########################################
	 */
	
	private static MetroLine[] parseMetroLineArray(String line) {
		String[] aux = parseStringArray(line);
		
		MetroLine[] ret = new MetroLine[aux.length];
		
		for(int i = 0; i < aux.length; i++)
			ret[i] = MetroLine.parseLinha(aux[i]);
		
		return ret;
	}
	
	private static String[] parseStringArray(String line) {
		// Take out first and last chars
		line = line.substring(1, line.length() - 1);
		// Clean spaces
		line = line.replace(" ", "");
		// Split
		return line.split(",");
	}
	
	
}
