package metroapi.util;

public class TokenGenerationException extends Exception {

	private static final long serialVersionUID = 1L;

	public TokenGenerationException() {
		super("An API token failed to be generated.");
	}
}
