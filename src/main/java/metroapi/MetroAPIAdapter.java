package metroapi;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Base64;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import org.json.JSONArray;
import org.json.JSONObject;

import metroapi.data.Day;
import metroapi.data.Destination;
import metroapi.data.MetroLine;
import metroapi.data.MetroLineState;
import metroapi.data.Stop;
import metroapi.data.StopTrainTimes;
import metroapi.data.TrainFrequency;
import metroapi.util.JSONConverter;
import metroapi.util.Pair;
import metroapi.util.TokenGenerationException;

/**
 * 
 *  API adapter for EstadoServicoML 1.0.1 available at 
 * https://api.metrolisboa.pt/store/apis/info?name=EstadoServicoML&version=1.0.1&provider=admin#/tab3.
 * 
 * @author Diogo F. P. Barros
 * 
 * @version 2.0
 *
 */
public class MetroAPIAdapter {
	private static MetroAPIAdapter INSTANCE;
	private static String URL;
	
	private static final String PROPERTIES_PATH = "config.properties";
	private static String KEY;
	
	private String accessToken;
	private long accessTokenGeneratedTime;
	private long accessTokenExpirationTime;
	
	private MetroAPIAdapter() throws FileNotFoundException, IOException {
		loadConfig();
	}
	
	private void loadConfig() throws FileNotFoundException, IOException {
		Properties props = new Properties();
		
		loadOrCreateConfigFile(props);
		
		String key = props.getProperty("CONSUMER_KEY");
		String secret = props.getProperty("CONSUMER_SECRET");
		KEY = Base64.getEncoder().encodeToString((key + ":" + secret).getBytes());
		
		URL = props.getProperty("API_URL");
	}
	
	private void loadOrCreateConfigFile(Properties props) throws FileNotFoundException, IOException {
		File f = new File(PROPERTIES_PATH);
		
		if(!f.exists()) {
			PrintWriter pw = new PrintWriter(f);
			
			pw.println("CONSUMER_KEY=");
			pw.println("CONSUMER_SECRET=");
			pw.println("API_URL=https://api.metrolisboa.pt:8243/estadoServicoML/1.0.1");
			
			pw.close();
		}
		
		props.load(new FileReader(f));
	}

	/*
	 * ######################################## ESTADO LINHA ########################################
	 */
	
	/**
	 * Permite obter o estado da circulacao de todas as linhas do Metro.
	 * @return null em caso de erro
	 * @throws TokenGenerationException 
	 */
	public List<MetroLineState> getEstadoLinhaTodos() throws TokenGenerationException {
		checkAccessToken();
		
		String url = URL + "/estadoLinha/todos";

		ArrayList<MetroLineState> ret = new ArrayList<>();

		JSONObject json = getAPIRequest(url);
		
		if(json == null || (json != null && json.getInt("codigo") != 200))
			return null;
		
		JSONObject response = json.getJSONObject("resposta");

		for(MetroLine linha : MetroLine.values())
			ret.add(new MetroLineState(linha, response.getString(linha.toString().toLowerCase())));

		return ret;
	}
	
	/**
	 * Permite obter o estado da circulacao de uma determinada linha do Metro.
	 * @param linha
	 * @return null em caso de erro
	 * @throws TokenGenerationException 
	 */
	public MetroLineState getEstadoLinha(MetroLine linha) throws TokenGenerationException {
		checkAccessToken();
		
		String url = URL + "/estadoLinha/" + linha;

		JSONObject json = getAPIRequest(url);
		
		if(json == null || (json != null && json.getInt("codigo") != 200))
			return null;
		
		JSONObject response = json.getJSONObject("resposta");

		return new MetroLineState(linha, response.getString(linha.toString().toLowerCase()));
	}

	/*
	 * ######################################## TEMPO ESPERA ########################################
	 */
	
	/**
	 * Permite obter o tempo de espera para o pr�ximo comboio de todas as estacoes da rede do Metro no momento em que � chamado.
	 * @return null em caso de erro
	 * @throws TokenGenerationException 
	 */
	public List<StopTrainTimes> getTempoEsperaTodos() throws TokenGenerationException {
		checkAccessToken();
		
		String url = URL + "/tempoEspera/Estacao/todos";

		JSONObject json = getAPIRequest(url);
		
		if(json == null || (json != null && json.getInt("codigo") != 200))
			return null;
		
		JSONArray response = json.getJSONArray("resposta");

		return JSONConverter.convertJSONArrayToStationTimesList(response);
	}
	
	/**
	 * Permite obter o tempo de espera para o pr�ximo comboio de todas as estacoes de uma das linhas do Metro
	 *  no momento em que � chamado.
	 * @param linha
	 * @return null em caso de erro
	 * @throws TokenGenerationException 
	 */
	public List<StopTrainTimes> getTempoEsperaLinha(MetroLine linha) throws TokenGenerationException {
		checkAccessToken();
		
		String url = URL + "/tempoEspera/Linha/" + linha;

		JSONObject json = getAPIRequest(url);
		
		if(json == null || (json != null && json.getInt("codigo") != 200))
			return null;
		
		JSONArray response = json.getJSONArray("resposta");

		return JSONConverter.convertJSONArrayToStationTimesList(response);
	}
	
	/**
	 * Permite obter o tempo de espera para o proximo comboio por estacao.
	 * @param stop_id
	 * @return null em caso de erro
	 * @throws TokenGenerationException 
	 */
	public List<StopTrainTimes> getTempoEsperaEstacao(String stopID) throws TokenGenerationException {
		checkAccessToken();
		
		String url = URL + "/tempoEspera/Estacao/" + stopID;

		JSONObject json = getAPIRequest(url);
		
		if(json == null || (json != null && json.getInt("codigo") != 200))
			return null;
		
		JSONArray response = json.getJSONArray("resposta");

		return JSONConverter.convertJSONArrayToStationTimesList(response);
	}

	/*
	 * ######################################## INFO ESTACAO ########################################
	 */
	
	/**
	 * Permite obter a informacao standarizada de todas as estacoes do Metro (coordenadas de GPS, URL, ID da estacao, etc).
	 * @return null em caso de erro
	 * @throws TokenGenerationException 
	 */
	public List<Stop> getInfoEstacaoTodos() throws TokenGenerationException {
		checkAccessToken();
		
		String url = URL + "/infoEstacao/todos";

		JSONObject json = getAPIRequest(url);
		
		if(json == null || (json != null && json.getInt("codigo") != 200))
			return null;
		
		JSONArray response = json.getJSONArray("resposta");

		return JSONConverter.convertJSONArrayToEstacaoList(response);
	}
	
	/**
	 * Permite obter a informacao standarizada de uma estacao do Metro (coordenadas de GPS, URL, ID da estacao, etc).
	 * @param estacao 
	 * @return null em caso de erro
	 * @throws TokenGenerationException 
	 */
	public Stop getInfoEstacao(String stopID) throws TokenGenerationException {
		checkAccessToken();
		
		String url = URL + "/infoEstacao/" + stopID;

		JSONObject json = getAPIRequest(url);
		
		if(json == null || (json != null && json.getInt("codigo") != 200))
			return null;
		
		JSONArray response = json.getJSONArray("resposta");

		return JSONConverter.convertJSONObjectToEstacao(response.getJSONObject(0));
	}
	
	/*
	 * ######################################## INFO DESTINOS ########################################
	 */
	
	/**
	 * Permite obter a lista de todos os destinos poss�veis na rede do Metro.
	 * @return null em caso de erro
	 * @throws TokenGenerationException 
	 */
	public List<Destination> getInfoDestinosTodos() throws TokenGenerationException {
		checkAccessToken();
		
		String url = URL + "/infoDestinos/todos";

		JSONObject json = getAPIRequest(url);
		
		if(json == null || (json != null && json.getInt("codigo") != 200))
			return null;
		
		JSONArray response = json.getJSONArray("resposta");

		return JSONConverter.convertJSONArrayToDestinationList(response);
	}
	
	/*
	 * ######################################## INFO INTERVALOS ########################################
	 */
	
	public TrainFrequency getInfoIntervalosLinhaDiaHora(MetroLine linha, Day dia, String hora) throws TokenGenerationException {
		checkAccessToken();
		
		String url = URL + "/infoIntervalos/" + linha + "/" + dia + "/" + hora;

		JSONObject json = getAPIRequest(url);
		
		if(json == null || (json != null && json.getInt("codigo") != 200))
			return null;
		
		JSONArray response = json.getJSONArray("resposta");

		return JSONConverter.convertJSONObjectToTrainFrequency(response.getJSONObject(0));
	}
	
	public List<TrainFrequency> getInfoIntervalosLinhaDia(MetroLine linha, Day dia) throws TokenGenerationException {
		checkAccessToken();
		
		String url = URL + "/infoIntervalos/" + linha + "/" + dia;

		JSONObject json = getAPIRequest(url);
		
		if(json == null || (json != null && json.getInt("codigo") != 200))
			return null;
		
		JSONArray response = json.getJSONArray("resposta");

		return JSONConverter.convertJSONArrayToTrainFrequencyList(response);
	}
	
	/*
	 * ######################################## AUX ########################################
	 */
	
	private JSONObject getAPIRequest(String url) {
		return getJSONFromHttpUrlConnection(prepareHttpUrlConnection("GET",
				url,
				Arrays.asList(new Pair<String, String>("accept", "application/json"),
							  new Pair<String, String>("Authorization", "Bearer " + accessToken)),
				5000,
				5000));
	}
	
	private JSONObject getJSONFromHttpUrlConnection(HttpURLConnection con) {
		if(con == null)
			return null;
		
		return new JSONObject(getHTTPResponseContent(con));
	}
	
	private HttpURLConnection prepareHttpUrlConnection(String requestMethod, String url, List<Pair<String, String>> headersRequestProperty, int connectTimeout, int readTimeout) {
		try {
			// Open connection
			HttpURLConnection con;

			con = (HttpURLConnection) new URL(url).openConnection();

			// Set to POST, GET, ...
			con.setRequestMethod(requestMethod);

			// Set timeouts
			con.setConnectTimeout(connectTimeout);
			con.setReadTimeout(readTimeout);

			// Add headers
			for(Pair<String, String> p : headersRequestProperty)
				con.setRequestProperty(p.getFirst(), p.getSecond());

			return con;
		} catch (Exception e) {
			// e.printStackTrace();
		} 

		return null;
	}
	
	private String getHTTPResponseContent(HttpURLConnection con) {
		if(con == null)
			return null;
		
		try {
			int responseCode = con.getResponseCode(); 
			if(responseCode != 200)
			{
				System.out.println(responseCode);
				return null;
			}
				
			// Read response
			BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
			String inputLine;
			StringBuffer content = new StringBuffer();
			while ((inputLine = in.readLine()) != null) {
				content.append(inputLine);
			}
			in.close();

			// Close connection
			con.disconnect();

			return content.toString();
		} catch (Exception e) {
			// e.printStackTrace();
		}

		return null;
	}
	
	/*
	 * ######################################## ACCESS TOKEN ########################################
	 */
	
	private void checkAccessToken() throws TokenGenerationException {
		if(!isAccessTokenValid())
			generateAccessToken();
	}
	
	private boolean isAccessTokenValid() {
		return System.currentTimeMillis() - accessTokenGeneratedTime < accessTokenExpirationTime;
	}
	
	private void generateAccessToken() throws TokenGenerationException {
		String url = "https://api.metrolisboa.pt:8243/token";
		
		try {
			accessTokenGeneratedTime = System.currentTimeMillis();
			
			// Open connection
			HttpURLConnection con = prepareHttpUrlConnection("POST",
					url,
					Arrays.asList(new Pair<String, String>("Authorization", "Basic " + KEY)),
					5000,
					5000);	
			
			// Add parameters
			Map<String, String> parameters = new HashMap<>();
			parameters.put("grant_type", "client_credentials");
			
			con.setDoOutput(true);
			DataOutputStream out = new DataOutputStream(con.getOutputStream());
			out.writeBytes(getParamsString(parameters));
			out.flush();
			out.close();

			JSONObject json = getJSONFromHttpUrlConnection(con);

			// Close connection
			con.disconnect();
			
			accessToken = json.getString("access_token");
			accessTokenExpirationTime = json.getLong("expires_in") * 1000;

		} catch (Exception e) {
			// e.printStackTrace();
			throw new TokenGenerationException();
		}
	}

	// Taken from https://www.baeldung.com/java-http-request
	private static String getParamsString(Map<String, String> params) 
			throws UnsupportedEncodingException{
		StringBuilder result = new StringBuilder();

		for (Map.Entry<String, String> entry : params.entrySet()) {
			result.append(URLEncoder.encode(entry.getKey(), "UTF-8"));
			result.append("=");
			result.append(URLEncoder.encode(entry.getValue(), "UTF-8"));
			result.append("&");
		}

		String resultString = result.toString();
		return resultString.length() > 0
				? resultString.substring(0, resultString.length() - 1)
						: resultString;
	}
	
	/*
	 * ######################################## INSTANCE ########################################
	 */
	
	public static MetroAPIAdapter getInstance() throws FileNotFoundException, IOException {
		if (INSTANCE == null)
			INSTANCE = new MetroAPIAdapter();
		
		return INSTANCE;
	}
}
