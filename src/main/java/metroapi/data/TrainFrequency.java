package metroapi.data;

public class TrainFrequency {
	private final MetroLine line;
	
	private final String startTime;
	private final String endTime;
	private final String frequency;
	
	private final String ut;
	private final Day day;
	
	public TrainFrequency(MetroLine line, String startTime, String endTime, String frequency, String ut, Day day) {
		this.line = line;
		
		this.startTime = startTime;
		this.endTime = endTime;
		this.frequency = frequency;
		
		this.ut = ut;
		this.day = day;
	}

	public MetroLine getLine() {
		return line;
	}

	public String getStartTime() {
		return startTime;
	}

	public String getEndTime() {
		return endTime;
	}

	public String getFrequency() {
		return frequency;
	}

	public String getUt() {
		return ut;
	}

	public Day getDay() {
		return day;
	}

	@Override
	public String toString() {
		return "TrainFrequency [line: " + line +
				"; startTime: " + startTime +
				"; endTime: " + endTime +
				"; frequency: " + frequency +
				"; ut: " + ut +
				"; day: " + day + "]";
	}
}
