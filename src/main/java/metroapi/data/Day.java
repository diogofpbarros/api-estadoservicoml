package metroapi.data;

/**
 * 
 * @author Diogo F. P. Barros
 *
 */
public enum Day {
	WEEKDAYS("S"),
	WEEKEND_HOLYDAYS("F");
	
	private final String representation;
	
	private Day(String representation) {
		this.representation = representation;
	}
	
	public String getRepresentation() {
		return representation;
	}
	
	/**
	 * Parse a Day from a given represention.
	 * @param s
	 * @return Day represented by s (not case sensitive) 
	 * or null if none is represented by s
	 */
	public static Day parseDay(String s) {
		switch(s.toLowerCase()) {
		case "s":
			return WEEKDAYS;
		case "f":
			return WEEKEND_HOLYDAYS;
		default:
			return null;
		}
	}
}
