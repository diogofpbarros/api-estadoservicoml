package metroapi.data;

/**
 * 
 * Immutable class that represents the state of a given MetroLine
 * 
 * @author Diogo F. P. Barros
 *
 */
public class MetroLineState {
	private final MetroLine metroLine;
	private final String state;
	
	public MetroLineState(MetroLine metroLine, String state) {
		this.metroLine = metroLine;
		this.state = state;
	}
	
	public MetroLine getMetroLine() {
		return metroLine;
	}
	
	public String getState() {
		return state;
	}
	
	@Override
	public String toString() {
		return "LineState [" + metroLine + ": " + state + "]";
	}
}
