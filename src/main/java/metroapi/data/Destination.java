package metroapi.data;

/**
 * 
 * Class that represents a metro destination (possible ending station of a train trip).
 * 
 * @author Diogo F. P. Barros
 *
 */
public class Destination {
	private final int destinationID;
	private final String destinationName;
	
	public Destination(int destinationID, String destinationName) {
		this.destinationID = destinationID;
		this.destinationName = destinationName;
	}

	public int getDestinationID() {
		return destinationID;
	}

	public String getDestinationName() {
		return destinationName;
	}
	
	@Override
	public String toString() {
		return "Destination [destinationID: " + destinationID + "; destinationName: " + destinationName + "]";
	}
}
