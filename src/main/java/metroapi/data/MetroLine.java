package metroapi.data;

/**
 * 
 * Enum describing all existing metro lines by default.
 * 
 * @author Diogo F. P. Barros
 *
 */
public enum MetroLine {
	Amarela,
	Azul,
	Verde,
	Vermelha;
	
	/**
	 * Parse a Line from a given represention.
	 * @param s
	 * @return Line represented by s (not case sensitive) 
	 * or null if none is represented by s
	 */
	public static MetroLine parseLinha(String s) {
		switch(s.toLowerCase()) {
		case "amarela":
			return Amarela;
		case "azul":
			return Azul;
		case "verde":
			return Verde;
		case "vermelha":
			return Vermelha;
		default:
			return null;
		}
	}
}
