package metroapi.data;

/**
 * 
 * Class that represents an arriving train and estimated time of await.
 * 
 * @author Diogo F. P. Barros
 *
 */
public class TrainTime {
	private final int time;	// In seconds
	private final String train;
	
	public TrainTime(int time, String train) {
		this.time = time;
		this.train = train;
	}
	
	/**
	 * Time in seconds
	 * @return
	 */
	public int getTime() {
		return time;
	}
	
	public String getTrain() {
		return train;
	}

	@Override
	public String toString() {
		return "[time:" + getRep(time) + "; train:" + train + "]";
	}
	
	private static String getRep(int tempo) {
		return tempo / 60 +  "m" + tempo % 60 + "s";
	}
}
