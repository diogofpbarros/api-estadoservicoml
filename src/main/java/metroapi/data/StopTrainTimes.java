package metroapi.data;
import java.util.Arrays;

/**
 * 
 * Class that represents TrainTimes in a given metro station and dock.
 * 
 * @author Diogo F. P. Barros
 *
 */
public class StopTrainTimes {
	private final String stopID;
	private final String dock;
	private final String timestamp;
	
	private final TrainTime[] trainTimes;
	
	private final int destination;
	private final String sairServico;
	
	public StopTrainTimes(String stopID, String dock, String timestamp, int[] trainTimes, String[] trainNames, int destination, String sairServico) {
		this.stopID = stopID;
		this.dock = dock;
		this.timestamp = timestamp;
		
		this.trainTimes = new TrainTime[3];
		
		for(int i = 0; i < trainTimes.length; i++)
			this.trainTimes[i] = new TrainTime(trainTimes[i], trainNames[i]);
		
		this.destination = destination;
		this.sairServico = sairServico;
	}

	public String getStopID() {
		return stopID;
	}

	public String getDock() {
		return dock;
	}

	public String getTimestamp() {
		return timestamp;
	}

	public TrainTime[] getTrainTimes() {
		return trainTimes;
	}

	public int getDestination() {
		return destination;
	}

	public String getSairServico() {
		return sairServico;
	}

	@Override
	public String toString() {
		return "[stopID:" + stopID +
			   "; dock:" + dock + 
			   "; timestamp:" + timestamp +
			   "; trainTimes:" + Arrays.toString(trainTimes) +
			   "; destination:" + destination +
			   "; sairServico:" + sairServico + "]";
	}
	
	
}
