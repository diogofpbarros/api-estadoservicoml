package metroapi.data;

import java.util.Arrays;

/**
 * 
 * Immutable class that represents a metro Station.
 * 
 * @author Diogo F. P. Barros
 *
 */
public class Stop {
	private final String stopID;
	private final String stopName;
	private final MetroLine[] metroLines;
	private final String zoneID;
	
	private final String[] stopURL;
	
	private final double stopLat;
	private final double stopLon;
	
	public Stop(String stopID, String stopName, String zoneID, MetroLine[] metroLines, String[] stopURL, double stopLat, double stopLon) {
		this.stopID = stopID;
		this.stopName = stopName;
		this.zoneID = zoneID;
		this.metroLines = metroLines;
		
		this.stopURL = stopURL;
		
		this.stopLat = stopLat;
		this.stopLon = stopLon;
	}

	public String getStopID() {
		return stopID;
	}

	public String getStopName() {
		return stopName;
	}

	public String[] getStopURLs() {
		return stopURL;
	}

	public MetroLine[] getMetroLines() {
		return metroLines;
	}

	public String getZoneID() {
		return zoneID;
	}
	
	/**
	 * GPS latitude coordinates.
	 * @return
	 */
	public double getStopLat() {
		return stopLat;
	}
	
	/**
	 * GPS longitude coordinates.
	 * @return
	 */
	public double getStopLon() {
		return stopLon;
	}
	
	@Override
	public String toString() {
		return "[ stop_id: " + stopID + 
			   "; stop_name: " + stopName +
			   "; stop_lat: " + stopLat +
			   "; stop_lon: " + stopLon + 
			   "; stop_url: " + stopURL +
			   "; linha: " + metroLines + 
			   "; zone_id: " + zoneID + "]";
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + Arrays.hashCode(metroLines);
		result = prime * result + ((stopID == null) ? 0 : stopID.hashCode());
		long temp;
		temp = Double.doubleToLongBits(stopLat);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		temp = Double.doubleToLongBits(stopLon);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		result = prime * result + ((stopName == null) ? 0 : stopName.hashCode());
		result = prime * result + Arrays.hashCode(stopURL);
		result = prime * result + ((zoneID == null) ? 0 : zoneID.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Stop other = (Stop) obj;
		if (!Arrays.equals(metroLines, other.metroLines))
			return false;
		if (stopID == null) {
			if (other.stopID != null)
				return false;
		} else if (!stopID.equals(other.stopID))
			return false;
		if (Double.doubleToLongBits(stopLat) != Double.doubleToLongBits(other.stopLat))
			return false;
		if (Double.doubleToLongBits(stopLon) != Double.doubleToLongBits(other.stopLon))
			return false;
		if (stopName == null) {
			if (other.stopName != null)
				return false;
		} else if (!stopName.equals(other.stopName))
			return false;
		if (!Arrays.equals(stopURL, other.stopURL))
			return false;
		if (zoneID == null) {
			if (other.zoneID != null)
				return false;
		} else if (!zoneID.equals(other.zoneID))
			return false;
		return true;
	}
}
